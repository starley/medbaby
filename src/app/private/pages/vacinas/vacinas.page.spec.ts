import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { VacinasPage } from './vacinas.page';

describe('VacinasPage', () => {
  let component: VacinasPage;
  let fixture: ComponentFixture<VacinasPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VacinasPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(VacinasPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
