import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: () => import('./public/pages/home/home.module').then( m => m.HomePageModule)
  },
  {
    path: 'perfil',
    loadChildren: () => import('./private/pages/perfil/perfil.module').then( m => m.PerfilPageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./public/pages/auth/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'forgot',
    loadChildren: () => import('./public/pages/auth/forgot/forgot.module').then( m => m.ForgotPageModule)
  },
  {
    path: 'monitoramento',
    loadChildren: () => import('./private/pages/monitoramento/monitoramento.module').then( m => m.MonitoramentoPageModule)
  },
  {
    path: 'relatorio',
    loadChildren: () => import('./private/pages/relatorio/relatorio.module').then( m => m.RelatorioPageModule)
  },
  {
    path: 'vacinas',
    loadChildren: () => import('./private/pages/vacinas/vacinas.module').then( m => m.VacinasPageModule)
  },
  {
    path: 'exames',
    loadChildren: () => import('./private/pages/exames/exames.module').then( m => m.ExamesPageModule)
  },
  {
    path: 'consultas',
    loadChildren: () => import('./private/pages/consultas/consultas.module').then( m => m.ConsultasPageModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
